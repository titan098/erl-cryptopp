#ifndef PBKDF_H
#define PBKDF_H

#include "erl_nif.h"

ERL_NIF_TERM pbkdf2_hmac_sha1(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]);

#endif
