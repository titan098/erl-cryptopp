#include "pbkdf.h"

#include <pwdbased.h>
#include <hmac.h>
#include <sha.h>

using namespace CryptoPP;

void calculate_pbkdf(PasswordBasedKeyDerivationFunction &pbkdf, SecByteBlock &password, SecByteBlock &salt, SecByteBlock &derived, unsigned int i) {
	pbkdf.DeriveKey(derived, derived.size(), 0, password, password.size(), salt, salt.size(), i);
}

ERL_NIF_TERM pbkdf2_hmac_sha1(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]) {
	//get the buffer
	ErlNifBinary password, salt;
	int iterations, length;

	ERL_NIF_TERM out;

	if (!enif_inspect_binary(env, argv[0], &password)) {
		return enif_make_badarg(env);
	}

	if (!enif_inspect_binary(env, argv[1], &salt)) {
		return enif_make_badarg(env);
	}

	if (!enif_get_int(env, argv[2], &iterations)) {
		return enif_make_badarg(env);
	}

	if (!enif_get_int(env, argv[3], &length)) {
		return enif_make_badarg(env);
	}

	//create the PBKDF and the secbyte structures
	SecByteBlock myPassword(password.data, password.size);
	SecByteBlock mySalt(salt.data, salt.size);
	SecByteBlock derivedKey(length);	

	PKCS5_PBKDF2_HMAC<SHA1> pbkdf;

	calculate_pbkdf(pbkdf, myPassword, mySalt, derivedKey, iterations);

	//marshal the binary back to an output binary
	byte* outputBuffer = (byte*)enif_make_new_binary(env, length, &out);
	memcpy(outputBuffer, derivedKey.BytePtr(), length);

	//clear all of the other buffers for good measure
	memset(myPassword.BytePtr(), 0x00, myPassword.size());
	memset(mySalt.BytePtr(), 0x00, mySalt.size());
	memset(derivedKey.BytePtr(), 0x00, derivedKey.size());

	return out;
}
